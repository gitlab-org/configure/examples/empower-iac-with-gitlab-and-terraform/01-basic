# NOTE: the backend definition is crucial in this example.
#       It lets Terraform know that we don't want to have a 
#       local Terraform state, but instead a remote HTTP-based
#       state. The backend will be configured by the 
#       GitLab CI/CD template.
terraform {
  backend "http" {}
}