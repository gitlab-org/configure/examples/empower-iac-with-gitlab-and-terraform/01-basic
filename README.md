# Basic Terraform Example using CI/CD template

This project illustrates a very basic Terraform setup
which uses the [GitLab-managed Terraform state](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html)
and the [GitLab Terraform CI/CD template](https://docs.gitlab.com/ee/user/infrastructure/iac/#latest-terraform-template).